# Pomotodo

**Pomotodo** is a project management tool like trello adding a pomodoro add-on.

## Technologies

Pomotodo is developped with **Android Studio** and **Java**.

## Prerequisites

To execute our pomotodo application, you must have downloaded and installed Java( JRE or JDK ) on your computer.
If you don't have it, you can download and install it :
### WINDOWS : [Oracle 's website](https://www.oracle.com/fr/java/technologies/javase-downloads.html)
or
#### GNU/LINUX ( Debian ) :  OpenJDK, with the following command :

```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install openjdk-8-jre

```

## How to launch pomotodo

 You can launch pomotodo by :
 * Using Android Studio directly to launch app.
 * Downloading the .APK file   

## How to use pomotodo

**Pomotodo** is a very simple and smart application which can help you in your job each days ! 
In fact, you can organize and manage yourself with our application.
To begin, you can :
* Sign in with *Google connect.*
* Create and add a new project.
* Delete or modifiy a project.
* Add some *ToDo* to your project.

## Ideas and possible improvement
There are possible improvements and ideas... Your suggestions are welcome ! 
* A timer for yours tasks in pomotodo.
* Statistics on yours times, tasks and projects.
* Sharing your tasks with somebody.
* Create new users with login and password.
* Several possibilities to connect to pomotodo : not only with *Google sign in*.
* You could download the application on **F-Droid** platform : the official free and open-source PlayStore.
* Multilanguage.
 
## How to contribute ?
Contributions are welcome, and merge request also ! If you want to contact our team, you can write email to **pomotodo.contact@gmail.com** 

## Authors
* Carine VEYRAT
* Adrien MARTIN
* Vincent TSACALIDES
* Quentin GENET

## License : GPLv3 
![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Heckert_GNU_white.svg/200px-Heckert_GNU_white.svg.png) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png)

Nobody should be restricted by the software they use. There are four freedoms that every user should have:

* The freedom to use the software for any purpose,
* The freedom to change the software to suit your needs,
* The freedom to share the software with your friends and neighbors, and
* The freedom to share the changes you make.

