package fr.pomotodo.pomotodo;

public class Todo {
    private String name;
    private String Uid;
    private Long status;
    private String taskStatus;
    private String description;

    public Todo() {

    }

    public Todo(String name, String uid, Long status, String description) {

        this.name = name;
        this.Uid = uid;
        this.status = status;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return Uid;
    }

    public String getStatus() {
        if (status == 0) {
            taskStatus = "Non défini";
        } else if (status == 1) {
            taskStatus = "En cours";
        } else if (status == 2) {
            taskStatus = "En attente";
        } else if (status == 3) {
            taskStatus = "A tester";
        } else if (status == 4) {
            taskStatus = "Terminé";
        }

        return taskStatus;
    }

    public void setStatus(Long status) {
        this.status = status;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
