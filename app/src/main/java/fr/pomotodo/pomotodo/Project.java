package fr.pomotodo.pomotodo;

public class Project {
    private String name;
    private String Uid;
    private String description;

    public Project() {
    }

    public Project(String name, String uid, String description) {

        this.name = name;
        this.Uid = uid;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return Uid;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
