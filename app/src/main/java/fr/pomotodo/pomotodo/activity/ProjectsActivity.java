package fr.pomotodo.pomotodo.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.Project;
import fr.pomotodo.pomotodo.adapter.ProjectsRecyclerViewAdapter;

public class ProjectsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProjectsRecyclerViewAdapter adapter;
    ArrayList<Project> projectArrayList;
    private RecyclerView.LayoutManager layoutManager;
    SearchView searchView;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        recyclerView = findViewById(R.id.recyclerView);

        projectArrayList = new ArrayList<>();

        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);

        //ActionBar
        ActionBar actionBar = getSupportActionBar();
        if(signInAccount != null) {
            actionBar.setTitle(signInAccount.getDisplayName());
        }

        /*TODO: Ajouter un filtrage par statut*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpRecyclerView();
        loadDataFromFirebase();
    }

    private void loadDataFromFirebase() {
        db.collection("projects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        projectArrayList.clear();
                        for(DocumentSnapshot snapshot : task.getResult()) {
                            Project project = new Project(snapshot.getString("name"), snapshot.getId(), snapshot.getString("description"));
                            projectArrayList.add(project);
                        }
                        adapter = new ProjectsRecyclerViewAdapter(ProjectsActivity.this, projectArrayList);
                        recyclerView.setAdapter(adapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ProjectsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        Log.d("tag", e.getMessage());
                    }
                });
    }

    private void setUpRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProjectsRecyclerViewAdapter(ProjectsActivity.this, projectArrayList);
        recyclerView.setAdapter(adapter);

    }

    //ACTIONBAR
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.projects_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Search project by name or id");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                List<Project> mProjectList = new ArrayList<>();
                for(Project project: projectArrayList) {
                    String projectName = project.getName().toLowerCase();
                    String projectId = project.getUid().toLowerCase();

                    if (projectName.contains(newText) || projectId.contains(newText)) {
                        mProjectList.add(project);
                    }

                }
                adapter.setSearchOperation(mProjectList);
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addProject_menu:
                Intent addProjectIntent = new Intent(getApplicationContext(), AddProjectActivity.class);
                startActivity(addProjectIntent);
                break;
            case R.id.logout_menu:
                FirebaseAuth.getInstance().signOut();
                Intent logoutIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(logoutIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
