package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.Todo;
import fr.pomotodo.pomotodo.adapter.TodoListRecyclerViewAdapter;

public class UpdateProjectActivity extends AppCompatActivity {

    String projectName;
    String projectId;
    SharedPreferences preferences;
    EditText updateName;
    private Button updateProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateproject);

        preferences = getSharedPreferences("PROJECT_INFO", MODE_PRIVATE);

        projectName = preferences.getString("NAME", "Project not found");
        projectId = preferences.getString("ID", "Id not found");

        updateName = findViewById(R.id.updateProjectName_txt);
        updateProject = findViewById(R.id.updateProjectName_btn);

        updateName.setText(projectName);

        updateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String projectName = updateName.getText().toString();
                Log.d("NAME", projectName);

                if (projectName.isEmpty()) {
                    Toast.makeText(UpdateProjectActivity.this, "Champ vide", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    Log.d("ID", projectId);
                    Log.d("NAME", projectName);

                    db.collection("projects")
                            .document(projectId)
                            .update("name", projectName)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(UpdateProjectActivity.this, "Project successfully updated!", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(UpdateProjectActivity.this, "Error updating project", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });

        //ACTIONBAR
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Modifier le projet");
        actionBar.setSubtitle(projectName);
    }

    //ACTIONBAR
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.updateproject_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteProject_menu:

                AlertDialog.Builder deletePopup = new AlertDialog.Builder(UpdateProjectActivity.this);
                deletePopup.setTitle("Voulez-vous confirmer la suppression du projet ?");
                deletePopup.setMessage("Cette action est irréversible et supprimera l'ensemble des tâches liées au projet !");
                deletePopup.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();

                        db.collection("todolist")
                                .whereEqualTo("project_id", projectId)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        FirebaseFirestore db = FirebaseFirestore.getInstance();

                                        for(DocumentSnapshot snapshot : task.getResult()) {
                                            Log.d("snapshot", snapshot.getId());
                                            db.collection("todolist").document(snapshot.getId()).delete();
                                        }
                                        db.collection("projects").document(projectId)
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(UpdateProjectActivity.this, "Project successfully deleted!", Toast.LENGTH_SHORT).show();
                                                        Intent addIntent = new Intent(getApplicationContext(), ProjectsActivity.class);
                                                        startActivity(addIntent);
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Toast.makeText(UpdateProjectActivity.this, "Error deleting project", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(UpdateProjectActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                        Log.d("tag", e.getMessage());
                                    }
                                });
                    }
                });
                deletePopup.setNegativeButton("NON", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(UpdateProjectActivity.this, "Deletion canceled", Toast.LENGTH_SHORT).show();
                    }
                });

                deletePopup.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}