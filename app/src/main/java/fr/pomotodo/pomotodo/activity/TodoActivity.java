package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.Todo;

public class TodoActivity extends AppCompatActivity {

    SharedPreferences preferences;
    String todoId;
    String todoName;
    TextView taskTimer;
    TextView taskStatus;
    TextView taskDescription;
    private String todoStatus;
    private String todoDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        preferences = getSharedPreferences("TODO_INFO", MODE_PRIVATE);

        todoId = preferences.getString("ID", "ID not found");
        todoName = preferences.getString("NAME", "Todo not found");

        taskTimer = findViewById(R.id.taskTimer_txt);
        taskStatus = findViewById(R.id.taskStatus_txt);
        taskDescription = findViewById(R.id.taskDescription_txt);


        //ACTIONBAR
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(todoName);
        actionBar.setSubtitle(todoId);

        /*TODO: Ajouter le timer*/
    }

    //ACTIONBAR
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.todo_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.updateTask_menu:
                Intent addProjectIntent = new Intent(getApplicationContext(), UpdateTodoActivity.class);
                startActivity(addProjectIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadDataFromFirebase();
    }

    private void loadDataFromFirebase() {

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("todolist").document(todoId);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Todo todo = documentSnapshot.toObject(Todo.class);
                todoDescription = todo.getDescription();
                todoStatus = todo.getStatus();
                taskStatus.setText(todoStatus!=null?todoStatus:"Status undefined");
                taskDescription.setText(todoDescription!=null?todoDescription:"No description provided");
            }
        });
    }
}