package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.Todo;

public class UpdateTodoActivity extends AppCompatActivity {

    String todoName;
    String todoDescription;
    String todoId;
    String todoStatus;
    Long taskStatus;
    SharedPreferences preferences;
    EditText updateName;
    EditText updateDescription;
    Button updateTask;
    RadioGroup rgStatus;
    RadioButton btnStatus1, btnStatus2, btnStatus3, btnStatus4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatetodo);

        preferences = getSharedPreferences("TODO_INFO", MODE_PRIVATE);

        todoName = preferences.getString("NAME", "Task not found");
        todoId = preferences.getString("ID", "Id not found");

        updateName = findViewById(R.id.updateTodoName_txt);
        updateDescription = findViewById(R.id.updateTodoDescription_txt);
        updateTask = findViewById(R.id.updateTodo_btn);

        rgStatus = findViewById(R.id.rgStatus);
        btnStatus1 = findViewById(R.id.status1_radio);
        btnStatus2 = findViewById(R.id.status2_radio);
        btnStatus3 = findViewById(R.id.status3_radio);
        btnStatus4 = findViewById(R.id.status4_radio);

        updateName.setText(todoName);

        updateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String todoName = updateName.getText().toString();
                final String todoDescription = updateDescription.getText().toString();
                int checkedId = rgStatus.getCheckedRadioButtonId();
                Log.d("checked", String.valueOf(checkedId));
                if (checkedId == -1) {
                    Toast.makeText(getApplicationContext(), "Please choose a status", Toast.LENGTH_SHORT).show();
                } else {
                    findRadioButton(checkedId);
                }

                if (todoName.isEmpty()) {
                    Toast.makeText(UpdateTodoActivity.this, "Champ vide", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    
                    db.collection("todolist")
                            .document(todoId)
                            .update(
                                    "name", todoName,
                                    "description", todoDescription,
                                    "status", taskStatus
                            )
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(UpdateTodoActivity.this, "Task successfully updated!", Toast.LENGTH_SHORT).show();
                                    Intent updateIntent = new Intent(getApplicationContext(), TodoActivity.class);
                                    startActivity(updateIntent);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(UpdateTodoActivity.this, "Error updating task", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });

        //ACTIONBAR
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Modifier la tâche");
        actionBar.setSubtitle(todoName);
    }

    private void findRadioButton(int checkedId) {
        switch (checkedId) {
            case R.id.status1_radio:
                taskStatus = Long.valueOf(1);
                break;
            case R.id.status2_radio:
                taskStatus = Long.valueOf(2);
                break;
            case R.id.status3_radio:
                taskStatus = Long.valueOf(3);
                break;
            case R.id.status4_radio:
                taskStatus = Long.valueOf(4);
                break;
        }
    }

    private void setRadioButton(String todoStatus) {
        if(todoStatus == "En cours"){
            btnStatus1.setChecked(true);
        }else if (todoStatus == "En attente"){
            btnStatus2.setChecked(true);
        } else if (todoStatus == "A tester"){
            btnStatus3.setChecked(true);
        }else if (todoStatus == "Terminé"){
            btnStatus4.setChecked(true);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        loadDataFromFirebase();


    }

    private void loadDataFromFirebase() {

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("todolist").document(todoId);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Todo todo = documentSnapshot.toObject(Todo.class);
                todoDescription = todo.getDescription();
                todoStatus = todo.getStatus();
                updateDescription.setText(todoDescription);

                setRadioButton(todoStatus);
            }
        });
    }

    //ACTIONBAR
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.updatetodo_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteTask_menu:

                AlertDialog.Builder deletePopup = new AlertDialog.Builder(UpdateTodoActivity.this);
                deletePopup.setTitle("Voulez-vous confirmer la suppression de la tâche ?");
                deletePopup.setMessage("Cette action est irréversible et supprimera l'ensemble des données de la tâche !");
                deletePopup.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();

                        db.collection("todolist").document(todoId)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(UpdateTodoActivity.this, "Task successfully deleted!", Toast.LENGTH_SHORT).show();
                                        Intent addIntent = new Intent(getApplicationContext(), TodolistActivity.class);
                                        startActivity(addIntent);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(UpdateTodoActivity.this, "Error deleting task", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                deletePopup.setNegativeButton("NON", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(UpdateTodoActivity.this, "Deletion canceled", Toast.LENGTH_SHORT).show();
                    }
                });

                deletePopup.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}