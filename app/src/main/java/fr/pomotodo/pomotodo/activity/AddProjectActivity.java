package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import fr.pomotodo.pomotodo.R;

public class AddProjectActivity extends AppCompatActivity {

    private EditText addProjectName;
    private EditText addProjectDescription;
    private Button addProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproject);

        //ACTIONBAR
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Ajouter un projet");

        addProjectName = findViewById(R.id.addProjectName_txt);
        addProjectDescription = findViewById(R.id.addProjectDescription_txt);
        addProject = findViewById(R.id.addProjectName_btn);

        addProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String projectName = addProjectName.getText().toString();
                final String projectDescription = addProjectDescription.getText().toString();

                if (projectName.isEmpty()) {
                    Toast.makeText(AddProjectActivity.this, "Champ vide", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    HashMap<String, Object> project = new HashMap<>();
                    project.put("name", projectName);
                    project.put("status", 1);
                    project.put("description", projectDescription);
                    project.put("team", null);

                    db.collection("projects").add(project).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if(task.isSuccessful()) {
                                Toast.makeText(AddProjectActivity.this, "Projet " + projectName + " ajouté !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    Intent intent = new Intent(getApplicationContext(), ProjectsActivity.class);
                    startActivity(intent);

                }
            }
        });

    }
}