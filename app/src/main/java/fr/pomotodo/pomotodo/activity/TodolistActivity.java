package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import fr.pomotodo.pomotodo.Project;
import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.Todo;
import fr.pomotodo.pomotodo.adapter.TodoListRecyclerViewAdapter;

public class TodolistActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TodoListRecyclerViewAdapter adapter;
    ArrayList<Todo> todoArrayList;
    private RecyclerView.LayoutManager layoutManager;
    String projectId;
    String projectName;
    SharedPreferences preferences;


    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist);

        recyclerView = findViewById(R.id.recyclerView);

        todoArrayList = new ArrayList<>();

        preferences = getSharedPreferences("PROJECT_INFO", MODE_PRIVATE);

        projectId = preferences.getString("ID", "ID not found");
        projectName = preferences.getString("NAME", "Todo not found");



        //ACTIONBAR
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(projectName);
        actionBar.setSubtitle(projectId);

        /*TODO: Ajouter un filtrage par statut*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpRecyclerView();
        loadDataFromFirebase();
    }

    private void loadDataFromFirebase() {

        db.collection("todolist")
                .whereEqualTo("project_id", projectId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        todoArrayList.clear();
                        for(DocumentSnapshot snapshot : task.getResult()) {
                            Todo todo = new Todo(snapshot.getString("name"), snapshot.getId(), snapshot.getLong("status"), snapshot.getString("description"));
                            todoArrayList.add(todo);
                        }
                        adapter = new TodoListRecyclerViewAdapter(TodolistActivity.this, todoArrayList);
                        recyclerView.setAdapter(adapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(TodolistActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        Log.d("tag", e.getMessage());
                    }
                });
    }

    private void setUpRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TodoListRecyclerViewAdapter(TodolistActivity.this, todoArrayList);
        recyclerView.setAdapter(adapter);

    }

    //ACTIONBAR
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.todolist_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Search todo by name or id");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                List<Todo> mTodolistList = new ArrayList<>();
                for(Todo todo: todoArrayList) {
                    String todolistName = todo.getName().toLowerCase();
                    String todolistId = todo.getUid().toLowerCase();

                    if (todolistName.contains(newText) || todolistId.contains(newText)) {
                        mTodolistList.add(todo);
                    }

                }
                adapter.setSearchOperation(mTodolistList);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addTodo_menu:
                Intent addIntent = new Intent(getApplicationContext(), AddTodoActivity.class);
                startActivity(addIntent);
                break;
            case R.id.updateProject_menu:
                Intent settingIntent = new Intent(getApplicationContext(), UpdateProjectActivity.class);
                startActivity(settingIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}