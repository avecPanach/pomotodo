package fr.pomotodo.pomotodo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import fr.pomotodo.pomotodo.R;

public class AddTodoActivity extends AppCompatActivity {

    private EditText addTodoName;
    private EditText addTodoDescription;
    private Button addTodo;
    String projectId;
    String projectName;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addtodo);

        //ACTIONBAR
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Ajouter une tâche");

        addTodoName = findViewById(R.id.addTodoName_txt);
        addTodo = findViewById(R.id.addTodoName_btn);
        addTodoDescription = findViewById(R.id.addTodoDescription_txt);

        preferences = getSharedPreferences("PROJECT_INFO", MODE_PRIVATE);

        projectId = preferences.getString("ID", "ID not found");
        projectName = preferences.getString("NAME", "Project not found");

        addTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String todoName = addTodoName.getText().toString();
                final String todoDescription = addTodoDescription.getText().toString();

                if (todoName.isEmpty()) {
                    Toast.makeText(AddTodoActivity.this, "Champ vide", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    HashMap<String, Object> todo = new HashMap<>();
                    todo.put("project_id", projectId);
                    todo.put("name", todoName);
                    todo.put("status", 1);
                    todo.put("description", todoDescription);

                    db.collection("todolist").add(todo).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if(task.isSuccessful()) {
                                Toast.makeText(AddTodoActivity.this, "Tâche " + todoName + " ajoutée !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

    }
}