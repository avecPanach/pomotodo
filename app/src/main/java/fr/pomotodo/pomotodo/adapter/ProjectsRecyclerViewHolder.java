package fr.pomotodo.pomotodo.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.pomotodo.pomotodo.ItemClickListener;
import fr.pomotodo.pomotodo.R;

public class ProjectsRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public TextView projectName;
    public TextView projectDescription;

    private ItemClickListener itemClickListener;

    public ProjectsRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);

        projectName = itemView.findViewById(R.id.projectName_txt);
        projectDescription = itemView.findViewById(R.id.projectDescription_txt);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void  setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), true);
        return true;
    }
}
