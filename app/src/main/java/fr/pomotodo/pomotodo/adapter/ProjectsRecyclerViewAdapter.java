package fr.pomotodo.pomotodo.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.pomotodo.pomotodo.ItemClickListener;
import fr.pomotodo.pomotodo.Project;
import fr.pomotodo.pomotodo.activity.ProjectsActivity;
import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.activity.TodolistActivity;
import fr.pomotodo.pomotodo.activity.UpdateProjectActivity;

public class ProjectsRecyclerViewAdapter extends RecyclerView.Adapter<ProjectsRecyclerViewHolder> {

    ProjectsActivity projectsActivity;
    ArrayList<Project> projectArrayList;
    SharedPreferences sharedPreferences;

    public ProjectsRecyclerViewAdapter(ProjectsActivity projectsActivity, ArrayList<Project> projectArrayList) {
        this.projectsActivity = projectsActivity;
        this.projectArrayList = projectArrayList;

    }

    @NonNull
    @Override
    public ProjectsRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(projectsActivity.getBaseContext());

        View view = layoutInflater.inflate(R.layout.projects_row, parent, false);
        return new ProjectsRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectsRecyclerViewHolder holder, int position) {
        holder.projectName.setText(projectArrayList.get(position).getName());
        holder.projectDescription.setText(projectArrayList.get(position).getDescription());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                sharedPreferences = view.getContext().getSharedPreferences("PROJECT_INFO", Context.MODE_PRIVATE);
                try {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    Log.d("NAME", projectArrayList.get(position).getName());
                    editor.putString("NAME", projectArrayList.get(position).getName());
                    editor.putString("ID", projectArrayList.get(position).getUid());
                    editor.apply();
                    Log.d("SUCCES", "success");
                } catch (Exception e) {
                    Log.d("BUG", String.valueOf(e));
                }
                if (isLongClick) {
                    Intent updateIntent = new Intent(projectsActivity.getApplicationContext(), UpdateProjectActivity.class);
                    projectsActivity.startActivity(updateIntent);
                } else {
                    Intent intent = new Intent(projectsActivity.getApplicationContext(), TodolistActivity.class);
                    projectsActivity.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectArrayList.size();
    }

    public void setSearchOperation(List<Project> newList) {
        projectArrayList = new ArrayList<>();
        projectArrayList.addAll(newList);
        notifyDataSetChanged();
    }
}
