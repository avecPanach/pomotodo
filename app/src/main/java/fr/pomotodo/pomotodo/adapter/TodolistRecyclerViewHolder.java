package fr.pomotodo.pomotodo.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.BreakIterator;

import fr.pomotodo.pomotodo.ItemClickListener;
import fr.pomotodo.pomotodo.R;

public class TodolistRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public TextView todoName;
    public TextView todoDescription;
    public TextView todoStatus;

    private ItemClickListener itemClickListener;

    public TodolistRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);

        todoName = itemView.findViewById(R.id.todoName_txt);
        todoStatus = itemView.findViewById(R.id.todoStatus_txt);
        todoDescription = itemView.findViewById(R.id.todoDescription_txt);


        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void  setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);

    }

    @Override
    public boolean onLongClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), true);
        return true;
    }
}
