package fr.pomotodo.pomotodo.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.pomotodo.pomotodo.ItemClickListener;
import fr.pomotodo.pomotodo.Project;
import fr.pomotodo.pomotodo.Todo;
import fr.pomotodo.pomotodo.R;
import fr.pomotodo.pomotodo.activity.AddTodoActivity;
import fr.pomotodo.pomotodo.activity.TodoActivity;
import fr.pomotodo.pomotodo.activity.TodolistActivity;
import fr.pomotodo.pomotodo.activity.UpdateProjectActivity;
import fr.pomotodo.pomotodo.activity.UpdateTodoActivity;

public class TodoListRecyclerViewAdapter extends RecyclerView.Adapter<TodolistRecyclerViewHolder> {

    TodolistActivity todosActivity;
    ArrayList<Todo> todoArrayList;
    SharedPreferences sharedPreferences;

    public TodoListRecyclerViewAdapter(TodolistActivity todosActivity, ArrayList<Todo> todoArrayList) {
        this.todosActivity = todosActivity;
        this.todoArrayList = todoArrayList;
    }

    @NonNull
    @Override
    public TodolistRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(todosActivity.getBaseContext());

        View view = layoutInflater.inflate(R.layout.todolist_row, parent, false);
        return new TodolistRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodolistRecyclerViewHolder holder, int position) {
        holder.todoName.setText(todoArrayList.get(position).getName());
        holder.todoStatus.setText(todoArrayList.get(position).getStatus());
        holder.todoDescription.setText(todoArrayList.get(position).getDescription());


        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                sharedPreferences = view.getContext().getSharedPreferences("TODO_INFO", Context.MODE_PRIVATE);
                try {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NAME", todoArrayList.get(position).getName());
                    editor.putString("ID", todoArrayList.get(position).getUid());
                    editor.apply();
                } catch (Exception e) {
                    Toast.makeText(view.getContext(), "No information", Toast.LENGTH_SHORT).show();
                }
                if (isLongClick) {
                    Intent updateIntent = new Intent(todosActivity.getApplicationContext(), UpdateTodoActivity.class);
                    todosActivity.startActivity(updateIntent);
                } else {
                    Intent intent = new Intent(todosActivity.getApplicationContext(), TodoActivity.class);
                    todosActivity.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return todoArrayList.size();
    }

    public void setSearchOperation(List<Todo> newList) {
        todoArrayList = new ArrayList<>();
        todoArrayList.addAll(newList);
        notifyDataSetChanged();
    }
}
